package utils;

import models.Student;

import java.io.*;
import java.util.ArrayList;

public class StudentFileEplorer {
    public boolean writeStudents(String fileName, ArrayList<Student> students) {
        boolean flag = false;
        try {
            String folder = "";
            String[] folders = fileName.split("/");
            if (folders.length > 2)
                for (int i = 0; i < folders.length - 1; i++) {
                    folder += folders[i] + "/";
                }
            else if (folders.length == 2)
                folder = folders[0];
            else
                folder = fileName;
            File dirs = new File(folder);
            if (!dirs.exists()) {
                if (dirs.mkdirs())
                    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
                        oos.writeObject(students);
                        flag = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            } else {
                try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
                    oos.writeObject(students);
                    flag = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public ArrayList<Student> readStudents(String fileName) {
        ArrayList<Student> students = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            students = (ArrayList<Student>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    public void printStudents(ArrayList<Student> students) {
        for (Student student : students)
            System.out.println(student);
    }
}
