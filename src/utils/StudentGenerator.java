package utils;
/*

Задача 1. Создать класс студент с полями фамилия, имя, массив с оценками и поле со средним
значением оценки. Используя сериализацию записать в файл данные о 10 студентах и
впоследствии считать из файла и вывести информацию о них.

*/

import models.Student;

import java.util.ArrayList;
import java.util.Random;

public class StudentGenerator {
    static String[] firstNames = {"David", "Loren", "Ben", "Irakly", "Kate", "Bruce", "Peter"};
    static String[] lastNames = {"Dvir", "Parker", "Wayne", "Gandelman", "Rosenberg", "Slavskii"};
    static Random rnd = new Random();


    public String getFirstName() {
        return firstNames[rnd.nextInt(firstNames.length)];
    }

    public String getLastName() {
        return lastNames[rnd.nextInt(lastNames.length)];
    }

    public ArrayList<Student> makeStudents(int quantity) {
        ArrayList<Student> students = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            students.add(new Student());
        }
        return students;
    }

    public ArrayList<Integer> getGrades() {
        int count = rnd.nextInt((9 - 3) + 1) + 3;
        return gradesGenerator(count);
    }


    public ArrayList<Integer> getGrades(int count) {
        return gradesGenerator(count);
    }

    public ArrayList<Integer> gradesGenerator(int count) {
        ArrayList<Integer> grades = new ArrayList<>();

        for (int i = 0; i < count; i++)
            grades.add(rnd.nextInt((5 - 2) + 1) + 2);

        return grades;
    }
}
