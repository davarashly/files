package utils;

import java.io.File;
import java.io.FileFilter;

public class ExtensionFileFilter implements FileFilter {
    String extension;

    public ExtensionFileFilter(String extension) {
        this.extension = extension;
    }

    public boolean accept(File pathname) {
        if (!pathname.isFile())
            return false;

        if (pathname.getAbsolutePath().endsWith("." + extension))
            return true;

        return false;
    }
}
