import models.FileExplorer;

public class FileExplorerApp {
    public static void main(String[] args) {
        FileExplorer fe = new FileExplorer("src");
        fe.showAllFiles();

        System.out.println("-------\n");

        fe.newDir("temp_files");
        fe.newDir("trash");

        System.out.println("\n-------\n");

        fe.deleteDir("trash");
        fe.deleteDir("some folder");

        System.out.println("\n-------\n");

        fe.newFile("readme", "txt");

        System.out.println("\n-------\n");

        fe.showFiles("poop");
    }
}
