import models.Student;
import utils.StudentFileEplorer;
import utils.StudentGenerator;


import java.util.ArrayList;

public class StudentsToDat {
    public static void main(String[] args) {
        StudentGenerator sg = new StudentGenerator();
        StudentFileEplorer sfe = new StudentFileEplorer();
        String file = "src/tmp/students.dat";
        String fileName = file.split("/")[file.split("/").length - 1];

        ArrayList<Student> students = sg.makeStudents(10);
        if (students.size() != 0) {
            if (sfe.writeStudents(file, students))
                System.out.println("Students are written in the \"" + fileName + "\".\n");
            else
                System.out.println("File write error.\n");
        } else
            System.out.println("Nothing to write lol, create students first.");

        students = sfe.readStudents(file);

        sfe.printStudents(students);

    }
}
