package models;

import utils.StudentGenerator;

import java.io.Serializable;
import java.util.ArrayList;

import static features.Funcs.*;


public class Student implements Serializable {
    private Integer id;
    private Double averageGrade;

    private String first_name;
    private String last_name;
    private ArrayList<Integer> grades;

    public Student() {
        StudentGenerator sg = new StudentGenerator();
        this.first_name = sg.getFirstName();
        this.last_name = sg.getLastName();
        this.grades = sg.getGrades();
        this.averageGrade = averageCalculate(this.grades);
    }

    double averageCalculate(ArrayList<Integer> grades) {
        double average = 0;

        for (int grade : grades)
            average += grade;
        average /= grades.size();
        average = round(average, 2);

        return average;
    }

    public Student(String first_name, String last_name, ArrayList<Integer> grades) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.grades = grades;
        this.averageGrade = averageCalculate(this.grades);
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Integer> grades) {
        this.grades = grades;
    }


    public String toString() {
        String s = ("" +
                "First Name:       " + this.getFirst_name() + "\n" +
                "Last Name:        " + this.getLast_name() + "\n"
        );

        s += "Grades:           ";

        for (int i = 0; i < this.getGrades().size(); i++) {
            if (i != this.getGrades().size() - 1)
                s += this.getGrades().get(i) + ", ";
            else
                s += this.getGrades().get(i);
        }

        s += "\nAverage Grade:    " + this.getAverageGrade() + "\n";

        return s;
    }

    public Double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(Double averageGrade) {
        this.averageGrade = averageGrade;
    }
}