package models;

import utils.ExtensionFileFilter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import static features.TextColors.*;

public class FileExplorer {
    String directory;
    File file;

    public FileExplorer(String path) {
        this.directory = path;
        this.file = new File(path);
    }

    public void showAllFiles() {
        File[] files = file.listFiles();
        if (files.length != 0) {
            System.out.println(ANSI_RED + "All files in \"" + directory + "/\"." + ANSI_RESET + "\n");
            for (File file : files)
                if (file.isDirectory())
                    System.out.println(file.getName() + "/");
                else
                    System.out.println(file.getName());
        } else
            System.out.println(ANSI_RED + "Folder \"" + directory + "/\" is empty." + ANSI_RESET);
        System.out.println();
    }

    public void newDir(String newDirName) {
        String currPath = this.file.getAbsoluteFile() + "/" + newDirName;
        File newDir = new File(currPath);
        if (newDir.mkdir())
            System.out.println("Directory \"" + newDirName + "\" was successfully created.");
        else
            System.out.println("Directory \"" + newDirName + "\" is already exists.");
    }

    public void newFile(String fileName, String extension) {
        String file = fileName + "." + extension;
        String currPath = this.file.getAbsoluteFile() + "/" + file;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(currPath))) {
            System.out.println("File \"" + file + "\" was successfully created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteDir(String dirName) {
        String currPath = this.file.getAbsoluteFile() + "/" + dirName;
        File dirDelete = new File(currPath);
        if (dirDelete.delete())
            System.out.println("Directory \"" + dirName + "\" was successfully deleted.");
        else
            System.out.println("Directory \"" + dirName + "\" doesn't exist.");
    }

    public void showFiles(String extension) {
        File[] files = file.listFiles(new ExtensionFileFilter(extension));
        if (files.length != 0) {
            System.out.println(ANSI_RED + "All files in \"" + file.getName() + "/\" " +
                    "with extension \"" + extension + "\"." + ANSI_RESET + "\n");
            for (File file : files)
                System.out.println(file.getAbsolutePath());
        } else
            System.out.println(ANSI_RED + "Files in \"" + file.getName() + "/\" " +
                    "with extension \"" + extension + "\" not found." + ANSI_RESET + "\n");
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
